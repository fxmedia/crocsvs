// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Texture2DDynamic.h"
#include "Invitee.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
AInvitee::AInvitee()
{
 	// Turn off ticking since we dont need it.
	PrimaryActorTick.bCanEverTick = false;

	// Add mesh component
	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	HeadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HeadMesh"));
	FaceMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FaceMesh"));
	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyMesh"));
	EmojiFrontMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EmojiFrontMesh"));

	// Add widget component
	TextFront = CreateDefaultSubobject<UWidgetComponent>(TEXT("TextFront"));
	TextBack = CreateDefaultSubobject<UWidgetComponent>(TEXT("TextBack"));

	// Add light component
	SpotLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("SpotLight"));
	
	// Set root component
	SetRootComponent(RootSceneComponent);
	RootComponent = RootSceneComponent;
	
	// Set components
	HeadMesh->SetupAttachment(RootComponent);
	FaceMesh->SetupAttachment(RootComponent);
	BodyMesh->SetupAttachment(RootComponent);
	EmojiFrontMesh->SetupAttachment(RootComponent);
	TextFront->SetupAttachment(RootComponent);
	TextBack->SetupAttachment(TextFront);
	SpotLight->SetupAttachment(RootComponent);
	
	// Set default values
	HighlightOffset = 50.0f;
	EmojiTimer = 2.0f;

	EmojiFrontMesh->SetVisibility(false, true);
	//EmojiFront->SetVisibility(false, true);  // VERY IMPORTANT FOR PERFORMANCE!!!
	//EmojiFront->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 130.0f), FRotator(0.0f, 0.0f, 90.0f));
	
	TextFront->SetVisibility(false, true); // VERY IMPORTANT FOR PERFORMANCE!!!
	TextBack->SetVisibility(false, true);  // TODO: We shouldnt have to set this because of the line above, but it doenst seem to work?
	TextFront->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 270.0f), FRotator(0.0f, 90.0f, 0.0f));
	TextBack->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 180.0f, 0.0f));
	TextFront->SetDrawSize(FVector2D(412.0f, 216.0f));
	TextBack->SetDrawSize(FVector2D(412.0f, 216.0f));

	TextFront->SetWidgetClass(TextWidget);
	TextBack->SetWidgetClass(TextWidget);

	SpotLight->SetVisibility(false, true);  // VERY IMPORTANT FOR PERFORMANCE!!!
	SpotLight->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 300.0f), FRotator(270.0f, 0.0f, 0.0f), false);
	SpotLight->SetOuterConeAngle(20.0f);

}

// Called when the game starts or when spawned
void AInvitee::BeginPlay()
{
	Super::BeginPlay();

	// We have to have an EmojiWidget, TextWidget, DynamicMaterialDefault and FallbackAvatar selected. Otherwise we will get strange crashes due to nullpointers when executing code.
	// To avoid confusion we log an Error message and quit the application.
	if (!EmojiWidget || !TextWidget || !DynamicMaterialDefault || FallbackAvatars.Num() == 0) {
		UE_LOG(LogTemp, Error, TEXT("AInvitee::BeginPlay No EmojiWidget, TextWidget, DynamicMaterialDefault or FallbackAvatar selected, exiting application"));
		GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
	}
}

void AInvitee::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);

	UE_LOG(LogTemp, Log, TEXT("AInvitee::OnConstruction_Implementation Executing in C++"));
	
	

	// By default we set a fallback Avatar, and download the avatar only if we have one
	FString AvatarUrl = InviteeData.avatar;
	SetFallbackAvatar();
	if (!AvatarUrl.IsEmpty()) {
		BPSetAvatar(AvatarUrl);
	}

	TextWidgetFront = CreateWidget<UInviteeTextWidget>(GetGameInstance(), TextWidget);
	TextWidgetBack = CreateWidget<UInviteeTextWidget>(GetGameInstance(), TextWidget);
	TextFront->SetWidget(TextWidgetFront);
	TextBack->SetWidget(TextWidgetBack);
}

// Called every frame
void AInvitee::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInvitee::SetFallbackAvatar_Implementation()
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::SetFallbackAvatar_Implementation Executing in C++"));
	
	// Create dynamic mat
	UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(DynamicMaterialDefault, FaceMesh);

	// Set parameters
	int32 RandomFallbackAvatarsIndex = UKismetMathLibrary::RandomIntegerInRange(0, FallbackAvatars.Num() - 1);
	UTexture2D* RandomFallbackAvatar = FallbackAvatars[RandomFallbackAvatarsIndex];
	DynamicMaterial->SetTextureParameterValue("BaseTexture", RandomFallbackAvatar);  // TODO: Remove this piece of hardcoded mess 

	// Set material
	FaceMesh->SetMaterial(0, DynamicMaterial);
}

void AInvitee::UpdateMe_Implementation(FReplyLiveInvitee invitee)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::UpdateMe_Implementation Executing in C++"));

	// Compare current data to new data
	bool isNameChanged = !(InviteeData.name == invitee.name);
	bool isAvatarChanged = !(InviteeData.avatar == invitee.avatar);

	if (isNameChanged) {
		SetName(invitee.name);
	}
	
	if (isAvatarChanged) {
		if (!invitee.avatar.IsEmpty()) {
			BPSetAvatar(invitee.avatar);
		}
		else {
			SetFallbackAvatar();
		}
	}

	InviteeData = invitee;
}

void AInvitee::SetPost_Implementation(const FString & Post)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::SetPost_Implementation Executing in C++"));
	
	// If we have a highlight, we do not want to do anything
	if (!hasHighlight) {
		TextWidgetFront->SetText(Post);
		TextWidgetBack->SetText(Post);

		SetName(InviteeData.name);

		// We never set this bool to false, because we always keep a post.
		hasPost = true;

		if (isPostHidden) {
			TextFront->SetVisibility(false, true);
		}
		else {
			TextFront->SetVisibility(true, true);
		}
	}
}

void AInvitee::SetName_Implementation(const FString & Name)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::SetName_Implementation Executing in C++"));
	TextWidgetFront->SetName(Name);
	TextWidgetBack->SetName(Name);
}

void AInvitee::SetEmoji(const FString & Emoji)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::SetEmoji Executing in C++"));
	ShowingEmoji = true;
	
	BPSetEmoji(Emoji);

	//EmojiWidgetFront->ExecuteAnimation();
	//EmojiWidgetBack->ExecuteAnimation();

	ShowEmoji(EmojiTimer);
}

void AInvitee::ResetEmoji()
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::ResetEmoji Executing in C++"));
	ShowingEmoji = false;

	//EmojiWidgetFront->HideEmoji();
	//EmojiWidgetBack->HideEmoji();

	ShowEmoji(0.1f);
}

void AInvitee::ShowEmoji(float ShowDuration)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::ShowEmoji Executing in C++"));
	//EmojiFront->SetVisibility(true, true);
	EmojiFrontMesh->SetVisibility(true, true);

	GetWorld()->GetTimerManager().SetTimer(EmojiHideHandle, this, &AInvitee::HideEmoji, ShowDuration, false);
}

void AInvitee::HideEmoji()
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::HideEmoji Executing in C++"));
	if (ShowingEmoji) {
		/* 
		We must reset the Emoji widget first. This is due to a functionality in UE4 Widgets where if we 
		make the WidgetComponent visible it will render 1 old frame of the UserWidget it contains. By
		using a reset texture here we make sure that old frame is completely transparent.

		Without doing so we would flash 1 frame of the old emoji. To try this; set the texture in
		UInviteeEmojiWidget.HideEmoji() to any texture that is not completely transparent. You will
		see this texture flash before the new emoji is shown.

		A concise explaination can be found here: 
		https://answers.unrealengine.com/questions/847283/view.html
		*/
		ResetEmoji();
	}
	else {
		//EmojiFront->SetVisibility(false, true);
		EmojiFrontMesh->SetVisibility(false, true);
	}
}

void AInvitee::HidePost_Implementation()
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::HidePost_Implementation Executing in C++"));

	// Only hide the post if we dont have a highlight
	if (!hasHighlight) {
		if (hasPost) {
			TextFront->SetVisibility(false, true);
		}
	}

	// Always set this flag, so we know we should be hidden if not highlighted anymore
	isPostHidden = true;
}

void AInvitee::ShowPost_Implementation()
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::ShowPost_Implementation Executing in C++"));

	if (hasPost) {
		TextFront->SetVisibility(true, true);
	}

	isPostHidden = false;
}

void AInvitee::HighlightPost_Implementation(const FString & Post)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::HighlightPost_Implementation Executing in C++"));

	// If we already have a highlight just change the post with what is selected for highlight
	if (hasHighlight) {
		SetPost(Post);
	}
	else {
		SetPost(Post);
		TextFront->SetVisibility(true, true);
		AddActorWorldOffset(FVector(0.0f, 0.0f, (0.0f + HighlightOffset)));
		SpotLight->SetVisibility(true, true);
		hasHighlight = true;
	}
}

void AInvitee::UnHighlightPost_Implementation()
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::UnHighlightPost_Implementation Executing in C++"));
	if (hasHighlight) {
		AddActorWorldOffset(FVector(0.0f, 0.0f, (0.0f - HighlightOffset)));
		SpotLight->SetVisibility(false, true);
		hasHighlight = false;
	}

	if (isPostHidden) {
		TextFront->SetVisibility(false, true);
	}
	
}

void AInvitee::BPSetAvatar_Implementation(const FString & AvatarUrl)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::BPSetAvatar_Implementation Executing in C++"));
}

void AInvitee::BPSetEmoji_Implementation(const FString & Emoji)
{
	UE_LOG(LogTemp, Log, TEXT("AInvitee::BPSetAvatar_Implementation Executing in C++"));
}