/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

#include "CSWOnlineSubsystem.h"

#define LOCTEXT_NAMESPACE "FCSWOnlineSubsystemModule"

void FCSWOnlineSubsystemModule::StartupModule()
{
	// This code will execute after your module is loaded into memorys; the exact timing is specified in the .uplugin file per-module
	
}

void FCSWOnlineSubsystemModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	
}

#undef LOCTEXT_NAMESPACE
///PLUGIN
IMPLEMENT_MODULE(FCSWOnlineSubsystemModule, CSWOnlineSubsystem)
///GAME MODULE
//IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, CSWOnlineSubsystem, "CSWOnlineSubsystem");