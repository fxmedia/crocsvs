/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

using UnrealBuildTool;

public class CSWOnlineSubsystem : ModuleRules
{
	public CSWOnlineSubsystem(ReadOnlyTargetRules Target) : base(Target)
	{
		//Override UnrealEngineBuildTool to speedup compilation times
        MinFilesUsingPrecompiledHeaderOverride = 1;
        bUseUnity = false;

        //INCLUDE WHAT YOU NEED CONVENTION ACTIVATION
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnforceIWYU = true;

        PublicIncludePaths.AddRange(
			new string[] {
				//"CSWOnlineSubsystem/Public"
				
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"CSWOnlineSubsystem/Private"
				
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
                "HTTP"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
                "ImageWrapper", //Identify image formats
                "RenderCore" //Create ue4 textures in runtime
				// ... add private dependencies that you statically link with here ...	
			}
			);
	}
}
