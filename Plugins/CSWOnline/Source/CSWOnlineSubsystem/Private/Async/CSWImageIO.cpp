/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

#include "Async/CSWImageIO.h"
#include "Modules/ModuleManager.h"
#include "RenderCore/Public/RenderUtils.h"
#include "Engine/Texture2D.h"
#include "Serialization/ObjectAndNameAsStringProxyArchive.h"
#include "Engine/TextureRenderTarget.h"
#include "Engine/Public/TextureResource.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Engine/Public/HighResScreenshot.h"
#include "Engine/Public/ImageUtils.h"
#include "ImageWrapper/Public/IImageWrapper.h"
#include "ImageWrapper/Public/IImageWrapperModule.h"
#include "Core/Public/PixelFormat.h"
#include "Misc/FileHelper.h"
#include "Async/Async.h"
//



/// Module loading is not allowed outside of the main thread, so we load the ImageWrapper module ahead of time.
//static IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(TEXT("ImageWrapper"));
//THIS LINE CAUSES CRASHES WHEN THE GAME IS PACKAGED!!!! IM PUTTING IT WHEN IS NEEDED INSTEAD

#pragma region Public

bool UCSWImageIO::CSWSaveRenderTargetToDisk(UTextureRenderTarget2D* RenderTarget, const FString& Path, const FString& FileName)
{
	/// Validation
	if (!FPaths::DirectoryExists(Path))
	{
		UE_LOG(LogTemp, Warning, TEXT("SaveRenderTargetToDisk: Directory doesn't exists"));
		return false;
	}
	if (!RenderTarget || FileName.Len() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("SaveRenderTargetToDisk: Parameters are invalid"));
		return false;
	}
	/// SaveRenderTargetToDisk
	return UCSWImageIO::SaveRenderTargetToDisk(RenderTarget, Path, FileName);
}

// void UCSWImageIO::AsyncSaveRenderTargetToDisk(UTextureRenderTarget2D* RenderTarget, const FString& Path, const FString& FileName, const FCSWOnImageSaveResponse& OnResponse)
// {
// 	/// Validation
// 	if (!FPaths::DirectoryExists(Path))
// 	{
// 		UE_LOG(LogTemp, Warning, TEXT("AsyncSaveRenderTargetToDisk: Directory doesn't exists"));
// 		return;
// 	}
// 	if (!RenderTarget || FileName.Len() <= 0)
// 	{
// 		UE_LOG(LogTemp, Warning, TEXT("AsyncSaveRenderTargetToDisk: Parameters are invalid"));
// 		return;
// 	}
// 	/// Start AsyncLoadImageFromDisk
// 	UCSWImageIO* ImageIO = UCSWImageIO::CreateCSWImageIO();
// 	ImageIO->InternalOnImageSaveResponse = OnResponse;
// 	ImageIO->SaveImageAsync(RenderTarget, Path, FileName);
// }

void UCSWImageIO::CSWLoadImageFromDisk(UObject* Outer, const FString& Path, const FString& FileName, const FString& FileExtension, UTexture2D*& Texture2D)
{
	/// Validation
	if (!FPaths::DirectoryExists(Path))
	{
		UE_LOG(LogTemp, Warning, TEXT("LoadImageFromDisk: Directory doesn't exists"));
		return;
	}
	if (!Outer || FileName.Len() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("LoadImageFromDisk: Parameters are invalid"));
		return;
	}
	/// LoadImageFromDisk
	const FString FullPath = Path + FileName + FileExtension;
	Texture2D = UCSWImageIO::LoadImageFromDisk(Outer, FullPath);
}

void UCSWImageIO::AsyncLoadImageFromDisk(UObject* Outer, const FString& Path, const FString& FileName, const FString& FileExtension, const FCSWOnImageLoadResponse& OnResponse)
{
	/// Validation
	if (!FPaths::DirectoryExists(Path))
	{
		UE_LOG(LogTemp, Warning, TEXT("AsyncLoadImageFromDisk: Directory doesn't exists"));
		return;
	}
	if (!Outer || FileName.Len() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("AsyncLoadImageFromDisk: Parameters are invalid"));
		return;
	}
	/// Start AsyncLoadImageFromDisk
	const FString FullPath = Path + FileName + FileExtension;
	UCSWImageIO* ImageIO = UCSWImageIO::CreateCSWImageIO();
	ImageIO->InternalOnImageLoadResponse = OnResponse;
	ImageIO->LoadImageAsync(Outer, FullPath);
}

#pragma endregion Public



#pragma region Private

UCSWImageIO* UCSWImageIO::CreateCSWImageIO()
{
	UCSWImageIO* ImageIO = NewObject<UCSWImageIO>();
	return ImageIO;
}

bool UCSWImageIO::SaveRenderTargetToDisk(UTextureRenderTarget2D* InRenderTarget, const FString& ImagePath, const FString& ImageName, bool bIsAsync)
{
	/// If path or RenderTarget is Invalid, return false
	if (!FPaths::DirectoryExists(ImagePath) || !InRenderTarget)
	{
		UE_LOG(LogTemp, Warning, TEXT("Path or InRenderTarget not found: %s"), *ImagePath);
		return false;
	}
	/// Init full path (imagepath + imagename)
	const FString& ImageFullPath = FPaths::Combine(*ImagePath, *ImageName) + TEXT(".png");

	/// Read pixels of InRenderTarget and save it inside an Array of FColor
	FTextureRenderTargetResource* RTResource = nullptr;
	if (bIsAsync)
	{
		RTResource = InRenderTarget->GetRenderTargetResource();
	}
	else
	{
		RTResource = InRenderTarget->GameThread_GetRenderTargetResource();
	}
	/// Init FReadSurfaceDataFlags
	FReadSurfaceDataFlags ReadPixelFlags(RCM_UNorm);
	ReadPixelFlags.SetLinearToGamma(true);
	/// Read pixels and store it inside OutBMP
	TArray<FColor> PixelDataArray;
	RTResource->ReadPixels(OUT PixelDataArray, ReadPixelFlags);

	///Size will be the size from the render target, is better to use 2^n values: i.e: 256x256
	FIntPoint DestSize(InRenderTarget->GetSurfaceWidth(), InRenderTarget->GetSurfaceHeight());

	///Compress the pixel data into an array of bytes
	TArray<uint8> CompressedPixelData;
	FImageUtils::CompressImageArray(DestSize.X, DestSize.Y, PixelDataArray, OUT CompressedPixelData);

	FFileHelper::SaveArrayToFile(CompressedPixelData, *ImageFullPath);
	
	//FHighResScreenshotConfig& HighResScreenshotConfig = GetHighResScreenshotConfig();
	//HighResScreenshotConfig.SaveImage(*ImageFullPath, PixelDataArray, DestSize, &ResultPath);
	return true;
}

UTexture2D* UCSWImageIO::LoadImageFromDisk(UObject* Outer, const FString& ImagePath, bool bIsAsync)
{
	/// Check if the file exists first
	if (!FPaths::FileExists(ImagePath))
	{
		UE_LOG(LogTemp, Warning, TEXT("File not found: %s"), *ImagePath);
		return nullptr;
	}

	/// Load the compressed byte data from the file
	TArray<uint8> FileData;
	if (!FFileHelper::LoadFileToArray(FileData, *ImagePath))
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to load file: %s"), *ImagePath);
		return nullptr;
	}
	/// Detect the image type using the ImageWrapper module
	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
	EImageFormat ImageFormat = ImageWrapperModule.DetectImageFormat(FileData.GetData(), FileData.Num());
	if (ImageFormat == EImageFormat::Invalid)
	{
		UE_LOG(LogTemp, Warning, TEXT("Unrecognized image file format: %s"), *ImagePath);
		return nullptr;
	}

	/// Create an image wrapper for the detected image format
	TSharedPtr<IImageWrapper> ImageWrapper = ImageWrapperModule.CreateImageWrapper(ImageFormat);
	if (!ImageWrapper.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to create image wrapper for file: %s"), *ImagePath);
		return nullptr;
	}

	/// Decompress the image data
	TArray<uint8> RawData;
	ImageWrapper->SetCompressed(FileData.GetData(), FileData.Num());
	ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, RawData);
	if (RawData.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to decompress image file, image file is empty %s"), *ImagePath);
		return nullptr;
	}

	/// Create the texture and upload the uncompressed image data
	FString TextureBaseName = TEXT("Texture_") + FPaths::GetBaseFilename(ImagePath);
	return CreateTexture(Outer, RawData, ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), EPixelFormat::PF_B8G8R8A8, FName(*TextureBaseName));
}

UTexture2D* UCSWImageIO::CreateTexture(UObject* Outer, const TArray<uint8>& PixelData, int32 InSizeX, int32 InSizeY, EPixelFormat InFormat /*= EPixelFormat::PF_B8G8R8A8*/, FName BaseName /*= NAME_None*/)
{
	// Create base texture
	UTexture2D* NewTexture = UTexture2D::CreateTransient(InSizeX, InSizeY, InFormat);

	// Return if invalid
	if (!NewTexture) return nullptr;

	// Copy data
	void* TextureData = NewTexture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
	FMemory::Memcpy(TextureData, PixelData.GetData(), PixelData.Num());
	NewTexture->PlatformData->Mips[0].BulkData.Unlock();

	/// Return Texture2D
	NewTexture->UpdateResource();
	return NewTexture;
}

#pragma endregion Private

#pragma region Async

void UCSWImageIO::SaveImageAsync(UTextureRenderTarget2D* InRenderTarget, const FString& ImagePath, const FString& ImageName)
{
	/// The asynchronous loading operation is represented by a Future, which will contain the result value once the operation is done.
	/// We store the Future in this object, so we can retrieve the result value in the completion callback below.
	SaveFuture = SaveRenderTargetToDiskAsync(InRenderTarget, ImagePath, ImageName, [this]()
	{
		/// This is the same Future object that we assigned above, but later in time.
		/// At this point, saving is done and the Future contains a value.
		if (SaveFuture.IsValid())
		{
			/// Notify listeners about the loaded texture on the game thread.
			AsyncTask(ENamedThreads::GameThread, [this]() { InternalOnImageSaveResponse.ExecuteIfBound(SaveFuture.Get()); });
		}
	});
}

TFuture<bool> UCSWImageIO::SaveRenderTargetToDiskAsync(UTextureRenderTarget2D* InRenderTarget, const FString& ImagePath, const FString& ImageName, TFunction<void()> CompletionCallback)
{
	/// Run the image loading function asynchronously through a lambda expression, capturing the ImagePath string by value.
	/// Run it on the thread pool, so we can load multiple images simultaneously without interrupting other tasks.
	return Async(EAsyncExecution::ThreadPool, [=]() { return SaveRenderTargetToDisk(InRenderTarget, ImagePath, ImageName, true); }, CompletionCallback);
}

void UCSWImageIO::LoadImageAsync(UObject* Outer, const FString& ImagePath)
{
	/// The asynchronous loading operation is represented by a Future, which will contain the result value once the operation is done.
	/// We store the Future in this object, so we can retrieve the result value in the completion callback below.
	LoadFuture = LoadImageFromDiskAsync(Outer, ImagePath, [this]()
	{
		/// This is the same Future object that we assigned above, but later in time.
		/// At this point, loading is done and the Future contains a value.
		if (LoadFuture.IsValid())
		{
			/// Notify listeners about the loaded texture on the game thread.
			AsyncTask(ENamedThreads::GameThread, [this]() { InternalOnImageLoadResponse.ExecuteIfBound(LoadFuture.Get()); });
		}
	});
}

TFuture<UTexture2D*> UCSWImageIO::LoadImageFromDiskAsync(UObject* Outer, const FString& ImagePath, TFunction<void()> CompletionCallback)
{
	/// Run the image loading function asynchronously through a lambda expression, capturing the ImagePath string by value.
	/// Run it on the thread pool, so we can load multiple images simultaneously without interrupting other tasks.
	return Async(EAsyncExecution::ThreadPool, [=]() { return LoadImageFromDisk(Outer, ImagePath, true); }, CompletionCallback);
}

#pragma endregion Async