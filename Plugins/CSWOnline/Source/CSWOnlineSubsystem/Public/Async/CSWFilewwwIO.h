/**
* Copyright (c) 2018 Cronofear Softworks, Inc. All Rights Reserved.
*
* Developed by Kevin Yabar Garces
*/

#pragma once

#include "HTTP/Public/Http.h"
#include "HTTP/Public/Interfaces/IHttpRequest.h"
#include "CSWFilewwwIO.generated.h"

USTRUCT(BlueprintType)
struct FCSWOnResponsewww
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "CSW|OnResponseWWW")
		FString URL;
	UPROPERTY(BlueprintReadOnly, Category = "CSW|OnResponseWWW")
		FString Path;
	UPROPERTY(BlueprintReadOnly, Category = "CSW|OnResponseWWW")
		FString FileName;
	UPROPERTY(BlueprintReadOnly, Category = "CSW|OnResponseWWW")
		FString FileExtension;
	UPROPERTY(BlueprintReadOnly, Category = "CSW|OnResponseWWW")
		TArray<FString> Params;
};

DECLARE_DYNAMIC_DELEGATE_TwoParams(FCSWOnFilewwwIOResponse, const bool, bWasSuccesful, FCSWOnResponsewww, ResponseData);
DECLARE_DYNAMIC_DELEGATE_ThreeParams(FCSWOnFilewwwIOProgress, const int32, BytesSent, const int32, BytesReceived, const int32, BytesTotal);

/**
 * Download/Upload a file from/to a URL provided from a cloud Service
 */
UCLASS(BlueprintType, Category = "HTTP")
class CSWONLINESUBSYSTEM_API UCSWFilewwwIO : public UObject
{
	GENERATED_BODY()

private:
	UCSWFilewwwIO();
	~UCSWFilewwwIO();

public:
	/**
	* Download a file using a URL that can be obtained from a back end provider.
	* Callbacks responses can be retrieved.
	* @param URL				URL where the file can be downloaded.
	* @param Path				Path where the file will be saved.
	* @param FileName			Name of the file to save.
	* @param FileExtension		Extension of the file to save.
	* @param Params				Custom parameters. It doesn't affect how the file is downloaded, these parameters are obtained when "OnResponse" is executed.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::WWWIO", meta = (DisplayName = "CSW::Async Download File", AutoCreateRefTerm="OnResponse,OnProgress,Params", AdvancedDisplay="Params"))
		static void AsyncDownloadFile(const FString& URL, const FString& Path, const FString& FileName, const FString& FileExtension, const TArray<FString>& Params,
			const FCSWOnFilewwwIOResponse& OnResponse, const FCSWOnFilewwwIOProgress& OnProgress);
	/**
	* Upload a file to using a URL that can be obtained from a back end provider.
	* Callbacks responses can be retrieved.w
	* @param URL				URL expecting an upload.
	* @param Path				Path where the file to upload is.
	* @param FileName			Name of the file to upload.
	* @param FileExtension		Extension of the file to upload.
	* @param Params				Custom parameters. It doesn't affect how the file is uploaded, these parameters are obtained when "OnResponse" is executed.
	* @param ContentType		Type of content that will be uploaded. Use "image/png" for .png images.
	*/
	UFUNCTION(BlueprintCallable, Category = "CSW|OnlineSubsystem::WWWIO", meta = (DisplayName = "CSW::Async Upload File", AutoCreateRefTerm = "OnResponse,OnProgress,Params", AdvancedDisplay = "ContentType, Params", ContentType = "*/*"))
		static void AsyncUploadFile(const FString& URL, const FString& Path, const FString& FileName, const FString& FileExtension, const FString& ContentType, const TArray<FString>& Params,
			const FCSWOnFilewwwIOResponse& OnResponse, const FCSWOnFilewwwIOProgress& OnProgress);

private:
	/**
	* Callback on download response.
	*/
	FCSWOnFilewwwIOResponse InternalOnResponse;
	/**
	* Callback on download progress.
	*/
	FCSWOnFilewwwIOProgress InternalOnProgress;
	/**
	* Content Type
	*/
	UPROPERTY()
		FString InContentType;
	/**
	* Concatenation of Path+FileName+FileExtension
	*/
	UPROPERTY()
		FString InFullPath;
	/**
	* OnResponseData (contains URL, FileName, FileExtension, etc)
	*/
	UPROPERTY()
		FCSWOnResponsewww OnResponseData;
	/**
	* Create a CreateCSWFilewwwIO that will handle the Async Download and Upload methods.
	*/
	UFUNCTION()
		static UCSWFilewwwIO* CreateCSWFilewwwIO();
	/**
	* Download a file using a URL and a Savepath (fullpath).
	*/
	UFUNCTION()
		UCSWFilewwwIO* DownloadFile();
	/**
	* Upload a file using URL and a Path.
	*/
	UFUNCTION()
		UCSWFilewwwIO* UploadFile();

private:

	void OnDownloadReady(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnDownloadProgress_Internal(FHttpRequestPtr Request, int32 BytesSent, int32 BytesReceived);

	void OnUploadReady(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnUploadProgress_Internal(FHttpRequestPtr Request, int32 BytesSent, int32 BytesReceived);
};
